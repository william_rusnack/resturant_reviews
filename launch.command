# #!/bin/sh

here="`dirname \"$0\"`"
echo "cd-ing to $here"
cd "$here" || exit 1

# python -m SimpleHTTPServer 8000 || python -m http.server 8000 &
python -m http.server 8000 &
python -m simpleHTTPServer 8000 &
open http://localhost:8000/resturant_reviews.html

$SHELL
